This is a notebook about R and Cartography. It gather several resources and examples collected on Internet. It ias also a drive test for **bookdown**.

 # How to clone this repo

    git clone https://framagit.org/Roelandtn/Rnotes.git

It is based on R Markdown and **bookdown** [github.com/rstudio/bookdown](https://github.com/rstudio/bookdown). Please see the page "Get Started" at https://bookdown.org/ for how to compile this document.

It is licensed with CC0 1.0 Universal licence. See [LICENCE.md](LICENCE.md) for more informations.
